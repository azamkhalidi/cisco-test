package com.azam.test.cisco.web.rest;

import com.amazonaws.regions.Regions;
import com.azam.test.cisco.service.InstanceService;
import com.azam.test.cisco.web.pagination.utils.PagedCollection;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.ResourceUtils;

import java.io.File;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@RunWith(MockitoJUnitRunner.class)
public class InstanceControllerTest {
    private MockMvc mockMvc;
    @Mock
    private InstanceService instanceService;
    @InjectMocks
    private InstanceController instanceController;

    private JacksonTester<PagedCollection> pagedCollectionJacksonTester;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(instanceController)
                .build();
        JacksonTester.initFields(this, new ObjectMapper());
    }

    @Test
    public void testGetPagedInstances() throws Exception {
        File responseFile = ResourceUtils.getFile("classpath:instance-response.json");
        //given
        ObjectMapper mapper = new ObjectMapper();
        PagedCollection responseObj = mapper.readValue(responseFile, PagedCollection.class);
        given(instanceService.getPagedInstances
                (any(Regions.class), anyString(), anyString(), anyInt(), anyInt()))
                .willReturn(responseObj);

        //when
        MockHttpServletResponse response = mockMvc.perform(
                get("/api/instances/region/US_EAST_1?order-by=NAME&direction=ASC&page=0&size=3")
                        .accept(MediaType.APPLICATION_JSON)
        ).andReturn().getResponse();

        // then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(response.getContentAsString()).isEqualTo(
                pagedCollectionJacksonTester.write(responseObj).getJson()
        );
    }

    @Test
    public void testIncorrectRegion() throws Exception {
        //when
        MockHttpServletResponse response = mockMvc.perform(
                get("/api/instances/region/US_EAST?order-by=NAME&direction=ASC&page=0&size=3")
                        .accept(MediaType.APPLICATION_JSON)
        ).andReturn().getResponse();

        // then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.BAD_REQUEST.value());
        assertThat(response.getContentAsString().contains("Invalid Region"));
    }

    @Test
    public void testIncorrectOrderBy() throws Exception {
        //when
        MockHttpServletResponse response = mockMvc.perform(
                get("/api/instances/region/US_EAST_1?order-by=NAM&direction=ASC&page=0&size=3")
                        .accept(MediaType.APPLICATION_JSON)
        ).andReturn().getResponse();

        // then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.BAD_REQUEST.value());
        assertThat(response.getContentAsString().contains("Invalid orderBy condition"));
    }

    @Test
    public void testIncorrectDirection() throws Exception {
        //when
        MockHttpServletResponse response = mockMvc.perform(
                get("/api/instances/region/US_EAST_1?order-by=NAME&direction=ASCE&page=0&size=3")
                        .accept(MediaType.APPLICATION_JSON)
        ).andReturn().getResponse();

        // then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.BAD_REQUEST.value());
        assertThat(response.getContentAsString().contains("Invalid sort condition"));
    }
}
