package com.azam.test.cisco.web.rest;

import com.azam.test.cisco.exception.AuthenticationFailedException;
import com.azam.test.cisco.security.jwt.TokenProvider;
import com.azam.test.cisco.service.AuthenticationService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@RunWith(MockitoJUnitRunner.class)
public class UserJWTControllerTest {
    public static final String ACCESS_KEY = "accessKey";
    public static final String SECRET_KEY = "secretKey";
    private static final String jwtMock = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhemFtIiwiYWNjZXNza2" +
            "V5IjoiQUtJQVNVUDdFSExDU1VXT0pMVU4iLCJzZWNyZXRrZXkiOiIyZGFQVzhCY296Z01PVmJ2eHZRSTRIS" +
            "kgrb0pmbVpGNWNKR29ubUxKIiwiZXhwIjoxNTU1Mjk3NjAzfQ.BN5kF0RwBaHxIuKIOkVRVBkVSlgBk5wnC_H" +
            "6heOaSMVMNYc4GY1hjqbU03XdmI0zW3Elrss5OuFMan2s4EsJ-g";
    private MockMvc mockMvc;
    @Mock
    private TokenProvider tokenProvider;
    @Mock
    private AuthenticationService authenticationService;
    @InjectMocks
    private UserJWTController userJWTController;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(userJWTController)
                .build();
    }

    @Test
    public void testAuthentication() throws Exception {
        UsernamePasswordAuthenticationToken authentication =
                new UsernamePasswordAuthenticationToken("test-user", "", Collections.emptyList());
        given(authenticationService.authenticate(ACCESS_KEY, SECRET_KEY))
                .willReturn(authentication);
        given(tokenProvider.createToken(authentication, ACCESS_KEY, SECRET_KEY))
                .willReturn(jwtMock);

        //when
        MockHttpServletResponse response =
                mockMvc.perform(post("/api/authenticate")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "\t\"accessKey\": \"accessKey\",\n" +
                                "\t\"secretKey\": \"secretKey\"\n" +
                                "}")
                        .accept(MediaType.APPLICATION_JSON))
                        .andReturn().getResponse();


        // then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(response.getContentAsString()).contains(jwtMock);
    }

    @Test
    public void testInvalidAuthentication() throws Exception {
        given(authenticationService.authenticate(ACCESS_KEY, SECRET_KEY))
                .willThrow(AuthenticationFailedException.class);

        //when
        MockHttpServletResponse response =
                mockMvc.perform(post("/api/authenticate")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "\t\"accessKey\": \"accessKey\",\n" +
                                "\t\"secretKey\": \"secretKey\"\n" +
                                "}")
                        .accept(MediaType.APPLICATION_JSON))
                        .andReturn().getResponse();


        // then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.UNAUTHORIZED.value());
    }
}
