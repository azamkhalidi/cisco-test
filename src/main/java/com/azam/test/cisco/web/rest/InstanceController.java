package com.azam.test.cisco.web.rest;

import com.amazonaws.regions.Regions;
import com.azam.test.cisco.exception.PreconditionException;
import com.azam.test.cisco.exception.ServiceErrorResponse;
import com.azam.test.cisco.service.InstanceService;
import com.azam.test.cisco.web.pagination.enums.Direction;
import com.azam.test.cisco.web.pagination.enums.OrderBy;
import com.azam.test.cisco.web.pagination.utils.PagedCollection;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/instances")
public class InstanceController {
    private final InstanceService instancesService;

    public InstanceController(InstanceService instancesService) {
        this.instancesService = instancesService;
    }

    @ApiOperation(value = "Return paged AWS instances for a given region",
            authorizations = @Authorization(value = "Bearer"))
    @GetMapping("region/{region}")
    public ResponseEntity<PagedCollection> getInstances(
            @PathVariable Regions region,
            @RequestParam("order-by") OrderBy orderBy,
            @RequestParam("direction") Direction direction,
            @RequestParam(value = "page", required = false) int page,
            @RequestParam(value = "size", required = false) int size) {

        return ResponseEntity.ok(instancesService
                .getPagedInstances(region, orderBy.getOrderByCode(), direction.getDirectionCode(), size, page));
    }


    @ExceptionHandler(PreconditionException.class)
    public ResponseEntity<ServiceErrorResponse> exceptionHandler(Exception ex) {
        ServiceErrorResponse pagingSortingErrorResponse = new ServiceErrorResponse();
        pagingSortingErrorResponse.setErrorCode(HttpStatus.PRECONDITION_FAILED.value());
        pagingSortingErrorResponse.setMessage(ex.getMessage());
        return new ResponseEntity<>(pagingSortingErrorResponse, HttpStatus.PRECONDITION_FAILED);
    }
}

