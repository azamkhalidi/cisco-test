package com.azam.test.cisco.web.pagination.enums;

import java.util.Arrays;
import java.util.stream.Collectors;

public enum OrderBy {
    NAME("name"),
    ID("id"),
    TYPE("type"),
    STATE("state"),
    PUBLIC_IP("publicIp"),
    PRIVATE_IP("privateIp"),
    AVAILIBILITY_ZONE("az");
    private String OrderByCode;

    OrderBy(String orderBy) {
        this.OrderByCode = orderBy;
    }

    public static boolean isValidOrder(final String orderByCode) {
        return Arrays.stream(OrderBy.values())
                .map(OrderBy::getOrderByCode)
                .collect(Collectors.toSet())
                .contains(orderByCode);
    }

    public String getOrderByCode() {
        return this.OrderByCode;
    }
}
