package com.azam.test.cisco.web.pagination.utils;

import lombok.Data;

import java.util.Collection;

@Data
public class PagedCollection {
    private Collection content;
    private int totalPages;
}
