package com.azam.test.cisco.web.vm;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class LoginVM {

    @NotNull
    private String accessKey;

    @NotNull
    private String secretKey;
}
