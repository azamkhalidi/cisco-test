package com.azam.test.cisco.web.pagination.enums;

import java.util.Arrays;
import java.util.stream.Collectors;

public enum Direction {
    ASC("asc"), DESC("desc");
    private final String directionCode;

    Direction(String direction) {
        this.directionCode = direction;
    }

    public static boolean isValidDirection(final String directionCode) {
        return Arrays.stream(Direction.values())
                .map(Direction::getDirectionCode)
                .collect(Collectors.toSet())
                .contains(directionCode);
    }

    public String getDirectionCode() {
        return this.directionCode;
    }
}
