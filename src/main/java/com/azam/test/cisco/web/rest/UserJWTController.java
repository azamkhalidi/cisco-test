package com.azam.test.cisco.web.rest;

import com.azam.test.cisco.exception.AuthenticationFailedException;
import com.azam.test.cisco.exception.ServiceErrorResponse;
import com.azam.test.cisco.security.jwt.JWTFilter;
import com.azam.test.cisco.security.jwt.TokenProvider;
import com.azam.test.cisco.service.AuthenticationService;
import com.azam.test.cisco.web.vm.LoginVM;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiOperation;
import lombok.Builder;
import lombok.Data;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("api")
public class UserJWTController {

    private final TokenProvider tokenProvider;
    private final AuthenticationService authenticationService;

    public UserJWTController(TokenProvider tokenProvider,
                             AuthenticationService authenticationService) {
        this.tokenProvider = tokenProvider;
        this.authenticationService = authenticationService;
    }

    @ApiOperation("Authenticate the user using AWS keys")
    @PostMapping("authenticate")
    public ResponseEntity<?> authenticate(@Valid @RequestBody LoginVM loginVM) {
        HttpHeaders httpHeaders = new HttpHeaders();
        String jwt = tokenProvider.createToken(
                authenticationService.authenticate(loginVM.getAccessKey(), loginVM.getSecretKey()),
                loginVM.getAccessKey(),
                loginVM.getSecretKey());
        httpHeaders.add(JWTFilter.AUTHORIZATION_HEADER, "Bearer " + jwt);
        return new ResponseEntity<>(JWTToken.builder().idToken(jwt).build(), httpHeaders, HttpStatus.OK);

    }

    @ExceptionHandler(AuthenticationFailedException.class)
    public ResponseEntity<ServiceErrorResponse> exceptionHandler(Exception ex) {
        ServiceErrorResponse authFailedErrorResponse = new ServiceErrorResponse();
        authFailedErrorResponse.setErrorCode(HttpStatus.UNAUTHORIZED.value());
        authFailedErrorResponse.setMessage(ex.getMessage());
        return new ResponseEntity<>(authFailedErrorResponse, HttpStatus.UNAUTHORIZED);
    }

    @Data
    @Builder
    public static class JWTToken {
        @JsonProperty("id_token")
        private String idToken;
    }
}
