package com.azam.test.cisco.security;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

@Data
public class User extends org.springframework.security.core.userdetails.User {
    private String awsAccessKey;
    private String awsSecretKey;

    public User(String username, String password, Collection<? extends GrantedAuthority> authorities,
                String awsAccessKey, String awsSecretKey) {
        super(username, password, authorities);
        this.awsAccessKey = awsAccessKey;
        this.awsSecretKey = awsSecretKey;
    }
}
