package com.azam.test.cisco.security;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

import java.util.Collections;

@Component("userDetailsService")
@Slf4j
public class AWSUserDetailsService implements UserDetailsService {

    @Override
    public UserDetails loadUserByUsername(final String login) {
        log.debug("Authenticating {}", login);

        return new User(login, "", Collections.emptyList());

    }
}
