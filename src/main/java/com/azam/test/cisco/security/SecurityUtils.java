package com.azam.test.cisco.security;

import com.amazonaws.auth.BasicAWSCredentials;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Optional;

public final class SecurityUtils {

    private SecurityUtils() {
    }

    public static Optional<BasicAWSCredentials> getCurrentUserAWSCredentials() {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        return Optional.ofNullable(securityContext.getAuthentication())
                .map(authentication -> {
                    if (authentication.getPrincipal() instanceof User) {
                        User user = (User) authentication.getPrincipal();
                        return new BasicAWSCredentials(user.getAwsAccessKey(), user.getAwsSecretKey());
                    }
                    return null;
                });
    }
}
