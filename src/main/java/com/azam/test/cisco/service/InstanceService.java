package com.azam.test.cisco.service;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.Reservation;
import com.azam.test.cisco.exception.PreconditionException;
import com.azam.test.cisco.security.SecurityUtils;
import com.azam.test.cisco.service.dto.InstanceDTO;
import com.azam.test.cisco.web.pagination.enums.Direction;
import com.azam.test.cisco.web.pagination.enums.OrderBy;
import com.azam.test.cisco.web.pagination.utils.PageUtils;
import com.azam.test.cisco.web.pagination.utils.PagedCollection;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class InstanceService {

    public List<InstanceDTO> getInstances(Regions region) {
        final AmazonEC2 ec2 = AmazonEC2ClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(SecurityUtils.getCurrentUserAWSCredentials().get()))
                .withRegion(region)
                .build();

        boolean done = false;

        DescribeInstancesRequest request = new DescribeInstancesRequest();

        List<InstanceDTO> instances = new ArrayList<>();
        while (!done) {
            DescribeInstancesResult response = ec2.describeInstances(request);

            for (Reservation reservation : response.getReservations()) {
                for (Instance instance : reservation.getInstances()) {
                    instances.add(InstanceDTO.builder()
                            .id(instance.getInstanceId())
                            .name(instance.getTags().get(0).getValue())
                            .type(instance.getInstanceType())
                            .state(instance.getState().getName())
                            .privateIp(instance.getPrivateIpAddress())
                            .publicIp(instance.getPublicIpAddress())
                            .availabilityZone(instance.getPlacement().getAvailabilityZone())
                            .build());
                }
            }

            request.setNextToken(response.getNextToken());

            if (response.getNextToken() == null) {
                done = true;
            }
        }
        return instances;
    }

    public PagedCollection getPagedInstances(Regions region, String orderBy, String direction,
                                                                int size, int page) {
        List<InstanceDTO> instances = getInstances(region);
        instances = instances.stream()
                .sorted(getInstancesComparator(orderBy, direction))
                .collect(Collectors.toList());
        List<List<InstanceDTO>> pages = PageUtils.getPages(instances, size);

        if(page >= pages.size()){
            throw new PreconditionException(String.format("Maximum %s pages available", pages.size()-1));
        }
        instances = pages.get(page);

        PagedCollection pagedCollection = new PagedCollection();
        pagedCollection.setContent(instances);
        pagedCollection.setTotalPages(pages.size());
        return pagedCollection;
    }

    private Comparator<InstanceDTO> getInstancesComparator(String orderBy, String direction) {
        return (o1, o2) -> {
            if (orderBy.equals(OrderBy.NAME.getOrderByCode())) {
                return direction.equals(Direction.ASC.getDirectionCode()) ?
                        o1.getName().compareToIgnoreCase(o2.getName()) :
                        o2.getName().compareToIgnoreCase(o1.getName());
            } else if (orderBy.equals(OrderBy.ID.getOrderByCode())) {
                return direction.equals(Direction.ASC.getDirectionCode()) ?
                        o1.getId().compareToIgnoreCase(o2.getId()) :
                        o2.getId().compareToIgnoreCase(o1.getId());
            } else if (orderBy.equals(OrderBy.STATE.getOrderByCode())) {
                return direction.equals(Direction.ASC.getDirectionCode()) ?
                        o1.getState().compareToIgnoreCase(o2.getState()) :
                        o2.getState().compareToIgnoreCase(o1.getState());
            } else if (orderBy.equals(OrderBy.TYPE.getOrderByCode())) {
                return direction.equals(Direction.ASC.getDirectionCode()) ?
                        o1.getType().compareToIgnoreCase(o2.getType()) :
                        o2.getType().compareToIgnoreCase(o1.getType());
            } else if (orderBy.equals(OrderBy.PRIVATE_IP.getOrderByCode())) {
                return direction.equals(Direction.ASC.getDirectionCode()) ?
                        o1.getPrivateIp().compareToIgnoreCase(o2.getPrivateIp()) :
                        o2.getPrivateIp().compareToIgnoreCase(o1.getPrivateIp());
            } else if (orderBy.equals(OrderBy.PUBLIC_IP.getOrderByCode())) {
                return direction.equals(Direction.ASC.getDirectionCode()) ?
                        o1.getPublicIp().compareToIgnoreCase(o2.getPublicIp()) :
                        o2.getPublicIp().compareToIgnoreCase(o1.getPublicIp());
            } else if (orderBy.equals(OrderBy.AVAILIBILITY_ZONE.getOrderByCode())) {
                return direction.equals(Direction.ASC.getDirectionCode()) ?
                        o1.getAvailabilityZone().compareToIgnoreCase(o2.getAvailabilityZone()) :
                        o2.getAvailabilityZone().compareToIgnoreCase(o1.getAvailabilityZone());
            } else {
                return o1.getName().compareToIgnoreCase(o2.getName());
            }
        };
    }
}
