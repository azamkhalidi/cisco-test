package com.azam.test.cisco.service;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagement;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagementClientBuilder;
import com.amazonaws.services.identitymanagement.model.User;
import com.azam.test.cisco.exception.AuthenticationFailedException;
import com.azam.test.cisco.security.AWSUserDetailsService;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class AuthenticationService {

    private final AWSUserDetailsService userDetailsService;

    public AuthenticationService(AWSUserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    public UsernamePasswordAuthenticationToken authenticate(String accessKey, String secretKey) {
        AmazonIdentityManagement identityManagement = AmazonIdentityManagementClientBuilder
                .standard()
                .withRegion(Regions.DEFAULT_REGION)
                .withCredentials(new AWSStaticCredentialsProvider
                        (new BasicAWSCredentials(accessKey, secretKey))).build();
        User awsUser;
        try {
            awsUser = identityManagement.getUser().getUser();
        } catch (Exception e) {
            throw new AuthenticationFailedException("Authentication with AWS failed");
        }

        UserDetails user = userDetailsService.loadUserByUsername(awsUser.getUserName());
        if (user == null) {
            throw new AuthenticationFailedException("User not found.");
        }
        Collection<? extends GrantedAuthority> authorities = user.getAuthorities();
        UsernamePasswordAuthenticationToken authentication =
                new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword(), authorities);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        return authentication;
    }
}
