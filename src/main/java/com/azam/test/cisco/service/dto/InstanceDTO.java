package com.azam.test.cisco.service.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class InstanceDTO {
    private String name;
    private String id;
    private String type;
    private String state;
    private String publicIp;
    private String privateIp;
    private String availabilityZone;
}
