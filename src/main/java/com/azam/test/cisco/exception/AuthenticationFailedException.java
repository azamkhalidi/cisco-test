package com.azam.test.cisco.exception;

public class AuthenticationFailedException extends RuntimeException {
    private String errorMessage;

    public AuthenticationFailedException() {
        super();
    }

    public AuthenticationFailedException(String errorMessage) {
        super(errorMessage);
        this.errorMessage = errorMessage;
    }

    @Override
    public String getMessage() {
        return errorMessage;
    }
}