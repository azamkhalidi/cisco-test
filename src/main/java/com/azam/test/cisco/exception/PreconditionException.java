package com.azam.test.cisco.exception;

public class PreconditionException extends RuntimeException {
    private String errorMessage;

    public PreconditionException() {
        super();
    }

    public PreconditionException(String errorMessage) {
        super(errorMessage);
        this.errorMessage = errorMessage;
    }

    @Override
    public String getMessage() {
        return errorMessage;
    }
}
