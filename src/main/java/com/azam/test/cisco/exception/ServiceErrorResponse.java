package com.azam.test.cisco.exception;

import lombok.Data;

@Data
public class ServiceErrorResponse {
    private int errorCode;
    private String message;
}
