**Public URL of the API**
http://cisco-azam-test.eu-west-2.elasticbeanstalk.com/swagger-ui.html

**Available Endpoints**

*  Login
```
POST /api/authenticate HTTP/1.1
{
	"accessKey": "xxxxxxxxxxxxxxxx",
	"secretKey": "xxxxxxxxxxxxxxxxxxxxxxxxx"
}
```
This returns an authentication token (JWT) which must be passed as an Authentication token to the subsequent requests.
The Authorization header is also passed in the response header.
The APIs are stateless. The validity of the jwt token can be managed by the property **security.jwt.property**. The jwt
token is signed using a key which can be managed by the property **security.jwt.base64-secret**


*  Response
```
{
    "id_token": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhemFtIiwiYWNjZXNza2V5IjoiQUtJQVNVUDdFSExDU1VXT0pMVU4iLCJzZWNyZXRrZXkiOiIyZGFQVzhCY296Z01PVmJ2eHZRSTRISkgrb0pmbVpGNWNKR29ubUxKIiwiZXhwIjoxNTU1Mjg1MzY3fQ.zspbq8euXfI6MiuD1audDJqaQgrqKZnJIpn8iM-A_6nYk_QtjigxLKZtPGR1AF3T_w8UOSBSWrJ350OPfgKJ7g"
}
```


*  Get Instances
```
GET /api/instances/region/US_EAST_1?order-by=NAME&direction=ASC&page=0&size=3 HTTP/1.1
Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhemFtIiwiYWNjZXNza2V5IjoiQUtJQVNVUDdFSExDU1VXT0pMVU4iLCJzZWNyZXRrZXkiOiIyZGFQVzhCY296Z01PVmJ2eHZRSTRISkgrb0pmbVpGNWNKR29ubUxKIiwiZXhwIjoxNTU1Mjg1MzY3fQ.zspbq8euXfI6MiuD1audDJqaQgrqKZnJIpn8iM-A_6nYk_QtjigxLKZtPGR1AF3T_w8UOSBSWrJ350OPfgKJ7g
```
Note: The token must be prepended with **Bearer**
```
direction *
Available values : ASC, DESC

order-by *
Available values : NAME, ID, TYPE, STATE, PUBLIC_IP, PRIVATE_IP, AVAILIBILITY_ZONE

page *
integer

region *
Available values : GovCloud, US_GOV_EAST_1, US_EAST_1, US_EAST_2, US_WEST_1, US_WEST_2, EU_WEST_1, EU_WEST_2, EU_WEST_3, EU_CENTRAL_1, EU_NORTH_1, AP_SOUTH_1, AP_SOUTHEAST_1, AP_SOUTHEAST_2, AP_NORTHEAST_1, AP_NORTHEAST_2, SA_EAST_1, CN_NORTH_1, CN_NORTHWEST_1, CA_CENTRAL_1

size *
integer
```

*  Response
```
{
    "content": [
        {
            "name": "xxxxx",
            "id": "i-0a71ee363b5eb0940",
            "type": "t2.micro",
            "state": "running",
            "publicIp": "xxxxxxxxxxxxx",
            "privateIp": "xxxxxxxxxxxxxx",
            "availabilityZone": "us-east-1a"
        },
        {
            "name": "xxxx",
            "id": "i-0deda0b19563eb17b",
            "type": "t2.small",
            "state": "running",
            "publicIp": "xxxxxxxxxxxx",
            "privateIp": "xxxxxxxxxxxx",
            "availabilityZone": "us-east-1a"
        },
        {
            "name": "xxxxxxxxxxxxx",
            "id": "i-046c23d25bc53fc6f",
            "type": "t2.micro",
            "state": "running",
            "publicIp": "xxxxxxxxxxxxx",
            "privateIp": "xxxxxxxxxxxx",
            "availabilityZone": "us-east-1a"
        }
    ],
    "totalPages": 8
}
```
This is a sample paged response displaying first page of size 3 sorted by name ascending.

**Local Setup**
This is a Spring boot project using java8 and maven for dependency management. 

*  Build the project
```
mvn clean install
```

*  Run in an embedded container
```
 java -jar target/cisco-1.0.4-SNAPSHOT.jar
```
This will start the application on port 8080.